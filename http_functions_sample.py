import urllib.parse, urllib.request, uuid, os, mimetypes, time

def filter_args(url, method, headers=None, data=None):
    if headers is None and data is None:
        urllib.request.Request(url, method=method)
    if headers is None:
        return urllib.request.Request(url, data=data, method=method)
    if data is None:
        return urllib.request.Request(url, headers=headers, method=method)
    else:
        return urllib.request.Request(url, headers=headers, method=method, data=data)

def send_request(url, method, headers=None, data=None):
    tries = 10
    request = filter_args(url, data=data, headers=headers, method=method)
    while tries > 0:
        try:
            response = urllib.request.urlopen(request)
            return response
        except urllib.error.URLError as error:
            tries -= 1
            print("Error: {}, {}".format(error, error.fp.read()))
            time.sleep(1)
    
def call(url, method, headers=None, files=None, body=None):
    if files:
        headers, body = encode_multipart(headers, files)
        return send_request(url, headers=headers, method=method, data=body)
    else:
        return send_request(url, headers=headers, method=method, data=body)

def encode_multipart(params, files):
    boundry = uuid.uuid4().hex
    lines = list()
    for key, val in params.items():
        if val is None: continue
        lines.append('--' + boundry)
        lines.append('Content-Disposition: form-data; name="%s"'%key)
        lines.extend([ '', val ])

    for key, uri in files.items():
        name = os.path.basename(uri)
        mime = mimetypes.guess_type(uri)[0] or 'application/octet-stream'
        file_contents = open(uri, 'rb')

        lines.append('--' + boundry)
        lines.append('Content-Disposition: form-data; name="{0}"; filename="{1}"'.format(key, name))
        lines.append('Content-Type: ' + mime)
        lines.append('')
        lines.append(file_contents.read())

        file_contents.close()

    lines.append('--%s--'%boundry)

    body = bytes()
    for l in lines:
        if isinstance(l, bytes): body += l + b'\r\n'
        else: body += bytes(l, encoding='utf8') + b'\r\n'

    headers = {}
    headers["Content-Type"] = 'multipart/form-data; boundary=' + boundry
    for param in params.keys():
        headers[param] = params[param]

    return headers, body

