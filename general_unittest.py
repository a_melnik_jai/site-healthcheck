import time
import unittest, urllib.request, json
from http_functions_sample import *


url = "https://russian-short-stories.ru/"


class Site_story_tab_healthcheck(unittest.TestCase):
    def test_ping(self):
        response = call(url=url, method="GET")
        self.assertEqual(response.status, 200)


    # def test_reader(self, fst_year="1900", snd_year="1930", offset=0):
    #     reader_errors = []
    #     for i in range(6):
    #         params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[title]=&orderBy=&limit={}&offset={}".format(
    #             fst_year, snd_year, "100", str(offset)
    #         )
    #         response = call(url=url + params, method="GET")
    #         if response.status == 429:
    #                 time.sleep(20)
    #         parsed = json.loads(response.read().decode("utf-8"))
    #         for i in range(len(parsed["rows"])):
    #             reader_response = call(url=url + "api/story/" + parsed["rows"][i]["_id"], method="GET")
    #             if reader_response.status == 429:
    #                 time.sleep(20)
    #             if reader_response.status != 200:
    #                 reader_errors.append(parsed["rows"][i]["title"])
    #         offset += 100
    #     if reader_errors:
    #         print(reader_errors)
    #     self.assertTrue(not reader_errors)


    def test_one_title_search(self, test_topics="будущее"):
        params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[topics]={}&orderBy=&limit={}&offset={}".format(
            "1900", "1930", urllib.parse.quote_plus(test_topics), "100", "0"
        )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        for i in range(len(parsed["rows"])):
            self.assertTrue(test_topics in parsed["rows"][i]['topics'].lower()) 
        self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_multiple_titles_search(self, test_topics="будущее, город, деньги, власть"):
            params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[topics]={}&orderBy=&limit={}&offset={}".format(
                "1900", "1930", urllib.parse.quote_plus(test_topics), "100", "0"
            )
            response = call(url=url + params, method="GET")
            parsed = json.loads(response.read().decode("utf-8"))
            for i in range(len(parsed["rows"])):
                self.assertTrue(test_topics in parsed["rows"][i]['topics'].lower()) 
            self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_find_title(self, test_title='кап'):
        params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[title]={}&orderBy=&limit={}&offset={}".format(
            "1900", "1930", urllib.parse.quote_plus(test_title), "10", "0"
        )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        for i in range(len(parsed["rows"])):
            self.assertTrue(test_title in parsed["rows"][i]['title'].lower()) 
        self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_find_author(self, test_name='чехов'):
        params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[authorName]={}&orderBy=&limit={}&offset={}".format(
            "1900", "1930", urllib.parse.quote_plus(test_name), "10", "0"
        )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        for i in range(len(parsed["rows"])):
            self.assertTrue(test_name in parsed["rows"][i]['authorName'].lower())
        self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_find_title_and_name(self, test_title="кап", test_name="Окунев"):
        params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[authorName]={}&filter[title]&orderBy=&limit={}&offset={}".format(
            "1900", "1930", urllib.parse.quote_plus(test_name), urllib.parse.quote_plus(test_title), "10", "0")
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        for i in range(len(parsed["rows"])):
            self.assertTrue(test_name in parsed["rows"][i]['authorName'].lower())
            self.assertTrue(test_title in parsed["rows"][i]['title'].lower())


    def test_years(self, fst_year="1900", snd_year="1930", offset=0):
        # total quantity of works = 534
        for i in range(6):
            params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[title]=&orderBy=&limit={}&offset={}".format(
                fst_year, snd_year, "100", str(offset)
            )
            response = call(url=url + params, method="GET")
            parsed = json.loads(response.read().decode("utf-8"))
            for i in range(len(parsed["rows"])):
                self.assertTrue(parsed["rows"][i]['year'] in range(int(fst_year), int(snd_year) + 1))
            offset += 100


    def test_pagination(self, fst_year="1900", snd_year="1930", offset=0):
        list_of_titles = []
        duplicated_titles = []
        # total quantity of works = 534
        for i in range(6):
            params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[title]=&orderBy=&limit={}&offset={}".format(
                fst_year, snd_year, "100", str(offset)
            )
            response = call(url=url + params, method="GET")
            parsed = json.loads(response.read().decode("utf-8"))
            for i in range(len(parsed["rows"])):
                if parsed["rows"][i]['title'].lower() in list_of_titles:
                    duplicated_titles.append(parsed["rows"][i]['title'].lower())
                #self.assertTrue(parsed["rows"][i]['title'].lower() not in list_of_titles)
                list_of_titles.append(parsed["rows"][i]['title'].lower())
            offset += 100
        # uncomment to get duplicates
        # if duplicated_titles:
        #     print(duplicated_titles)
        self.assertTrue(not duplicated_titles)


class Site_author_tab_healthcheck(unittest.TestCase):
    def test_find_author_surname(self, test_surname="правд"):
        params = "api/author?filter[deathYearRange][]={}&filter[deathYearRange][]={}&filter[birthYearRange][]={}&filter[birthYearRange][]={}&filter[author]={}&orderBy=&limit={}&offset={}".format(
                "1886", "2013", "1825", "1920", urllib.parse.quote_plus(test_surname), "100", "0"
            )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        for i in range(len(parsed["rows"])):
            self.assertTrue(test_surname in parsed["rows"][i]['name'].lower())
        self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_find_author_name(self, test_name="петр"):
            params = "api/author?filter[deathYearRange][]={}&filter[deathYearRange][]={}&filter[birthYearRange][]={}&filter[birthYearRange][]={}&filter[author]={}&orderBy=&limit={}&offset={}".format(
                    "1886", "2013", "1825", "1920", urllib.parse.quote_plus(test_name), "100", "0"
                )
            response = call(url=url + params, method="GET")
            parsed = json.loads(response.read().decode("utf-8"))
            for i in range(len(parsed["rows"])):
                self.assertTrue(test_name in parsed["rows"][i]['name'].lower())
            self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_author_year_range(self, death_fst_year="1933", death_snd_year="1946", birth_fst_year="1880",
                                birth_snd_year="1904", test_name="петр"):
            params = "api/author?filter[deathYearRange][]={}&filter[deathYearRange][]={}&filter[birthYearRange][]={}&filter[birthYearRange][]={}&filter[author]={}&orderBy=&limit={}&offset={}".format(
                    death_fst_year, death_snd_year, birth_fst_year, birth_snd_year, urllib.parse.quote_plus(test_name), "100", "0"
                )
            response = call(url=url + params, method="GET")
            parsed = json.loads(response.read().decode("utf-8"))
            for i in range(len(parsed["rows"])):
                self.assertTrue(test_name in parsed["rows"][i]['name'].lower())
            self.assertEqual(len(parsed["rows"]), parsed["count"])


    def test_find_images(self):
        params = "api/author?filter[deathYearRange][]={}&filter[deathYearRange][]={}&filter[birthYearRange][]={}&filter[birthYearRange][]={}&filter[author]={}&orderBy=&limit={}&offset={}".format(
                "1886", "2013", "1825", "1920", "", "150", "0"
            )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        # Not sure
        for i in range(len(parsed["rows"])):
            self.assertTrue(parsed["rows"][i]['photo'] is not None and
                            parsed["rows"][i]['photo'] != 0)
        self.assertEqual(len(parsed["rows"]), parsed["count"])


if __name__ == "__main__":
    unittest.main()
