import unittest, urllib.request, json
from http_functions_sample import *

url = "https://russian-short-stories.ru/"


class Site_healthcheck(unittest.TestCase):
    def test_ping(self):
        response = call(url=url, method="GET")
        self.assertEqual(response.status, 200)

    def test_story_search(self):
        params = "api/story?filter[yearRange][]={}&filter[yearRange][]={}&filter[topics]={}&orderBy=&limit={}&offset={}".format(
            "1900", "1930", urllib.parse.quote_plus("БУДУЩЕЕ"), "0", "10"
        )
        response = call(url=url + params, method="GET")
        parsed = json.loads(response.read().decode("utf-8"))
        self.assertEqual(len(parsed["rows"]), parsed["count"])
